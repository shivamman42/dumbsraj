﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DumbsRaj.domain.Models.Type;
using DumbsRaj.Application.Interfaces;
using DumbsRaj.Application.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DumbsRajAPI.Controllers
{
    //[Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class TypeController : ControllerBase
    {
        public readonly IType _type;

        public TypeController()
        {
            _type = new TypeServices();
        }

        // GET: api/Type
        [HttpGet("List")]
        public ActionResult List()
        {
            try
            {
                return Ok(_type.List());
            }
            catch (Exception ex)
            {

                throw;
            }

        }

        // GET: api/Type/5
        [HttpGet("getById/{Id}")]
        public ActionResult GetById(int Id)
        {
            try
            {
                return Ok(_type.GetById(Id));
            }
            catch (Exception)
            {
                throw;
            }
        }

        // POST: api/Type
        [HttpPost("Create")]
        public ActionResult Create([FromBody] TypesVM result)
        {
            try
            {
                _type.Create(result);
                return Ok(true);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        // PUT: api/Type/5
        [HttpPut("Update")]
        public bool Put([FromBody] TypesVM result)
        {
            try
            {
                _type.Update(result);
                return true;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("delete/{id}")]
        public bool Delete(int id)
        {
            try
            {
                _type.Delete(id);
                return true;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
