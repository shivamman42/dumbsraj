﻿using System.Threading.Tasks;
using Auth.Helpers;
using Auth.Interfaces;
using Auth.Models;
using Auth.Services;
using Authentication.Auth;
using AutoMapper;
using DumbsRaj.Auth.ViewModels;
using DumbsRaj.persistence.Models;
using DumbsRaj.persistence.Models.AppUser;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace DumbsRajAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountsController : ControllerBase
    {
        private readonly IAuthentication _auth;

        private DumbsRajContext _appDbContext;
        private UserManager<AppUser> _userManager;
        private IMapper _mapper;

        private readonly IJwtFactory _jwtFactory;
        private JwtIssuerOptions _jwtOptions;
        IOptions<JwtIssuerOptions> _options;

        public AccountsController(DumbsRajContext appDbContext, UserManager<AppUser> userManager, IMapper mapper,
            IJwtFactory jwtFactory, IOptions<JwtIssuerOptions> options)
        {
            _appDbContext = appDbContext;
            _userManager = userManager;
            _mapper = mapper;
            _jwtFactory = jwtFactory;

            _jwtOptions = options.Value;
            _options = options;
            this._auth = new AuthenticationServices(_userManager, _appDbContext, _mapper, _jwtFactory, _options);
        }

        [HttpPost("createUser")]
        public async Task<IActionResult> Post([FromBody]RegistrationViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            bool Identity = await _auth.CreateUser(model);

            if (Identity == false)
            {
                return BadRequest(ModelState);
            }
            else
            {
                return new OkObjectResult("Account created");
            }
        }

        [HttpPost("login")]
        public async Task<IActionResult> Post([FromBody]CredentialViewModel credentials)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var identity = await _auth.GetClaimsIdentity(credentials.UserName, credentials.Password);

            if (identity == null)
            {
                return BadRequest(Errors.AddErrorToModelState("login_failure", "Invalid username or password.", ModelState));
            }

            var jwt = await Tokens.GenerateJwt(identity, _jwtFactory, credentials.UserName, _jwtOptions, new JsonSerializerSettings { Formatting = Formatting.Indented });
            return new OkObjectResult(jwt);
        }
    }
}