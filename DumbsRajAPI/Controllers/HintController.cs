﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DumbsRaj.domain.Models;
using DumbsRaj.Application.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DumbsRajAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HintController : ControllerBase
    {
        public readonly HintServices _hint;

        public HintController()
        {
            _hint = new HintServices();
        }

        [HttpGet("list")]
        public ActionResult List() {
            try
            {
                return Ok(_hint.List());
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        [HttpPost("Create")]
        public ActionResult Create([FromBody]HintVM data) {
            try
            {
                _hint.Create(data);
                return Ok(true);
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        [HttpGet("getById/{Id}")]
        public ActionResult GetById(int Id) {
            try
            {
                return Ok(_hint.GetById(Id));
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        [HttpPut("Update")]
        public ActionResult Update([FromBody]HintVM data)
        {
            try
            {
                _hint.Update(data);
                return Ok(true);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}