﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DumbsRaj.Application.Interfaces;
using DumbsRaj.Application.Services;
using DumbsRaj.domain.Models.Type;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DumbsRajAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MovieController : ControllerBase
    {
        public readonly IMovie _movie;

        public MovieController()
        {
            _movie = new MovieServices();
        }

        [HttpGet("list")]
        public ActionResult List()
        {
            try
            {
                return Ok(_movie.List());
            }
            catch (Exception)
            {

                throw;
            }

        }

        [HttpGet("getById/{Id}")]
        public ActionResult GetById(int Id)
        {
            try
            {
                return Ok(_movie.GetById(Id));
            }
            catch (Exception)
            {
                throw;
            }
        }

        [HttpPost("Create")]
        public ActionResult Create([FromBody]  DumbsRaj.domain.Models.Movie.MoviesVM result)
        {
            try
            {
                return Ok(_movie.Create(result));
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        // PUT: api/Type/5
        [HttpPut("Update")]
        public bool Put([FromBody] DumbsRaj.domain.Models.Movie.MoviesVM result)
        {
            try
            {
                _movie.Update(result);
                return true;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("delete/{id}")]
        public bool Delete(int id)
        {
            try
            {
                _movie.Delete(id);
                return true;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
