﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DumbsRaj.domain.Models
{
    public class BaseVM
    {
        public int Id { get; set; }
        public DateTime AddedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
    }
}
