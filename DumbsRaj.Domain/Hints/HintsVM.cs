﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;


namespace DumbsRaj.domain.Models
{
    public class HintVM:BaseVM
    {
        public string Description { get; set; }
        public int QuestionId { get; set; }

        public QuestionsVM Question { get; set; }
        public string MediaUrl { get; set; }
    }
}
