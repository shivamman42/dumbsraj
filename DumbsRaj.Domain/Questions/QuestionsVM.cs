﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DumbsRaj.domain.Models
{
    public class QuestionsVM:BaseVM
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public int  UserId { get; set; }
        public ICollection<HintVM> Hints { get; set; }
    }
}
