﻿using Auth.Interfaces;
using Auth.Models;
using Authentication.Auth;
using AutoMapper;
using DumbsRaj.Auth.ViewModels;
using DumbsRaj.persistence.Models;
using DumbsRaj.persistence.Models.AppUser;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Auth.Services
{
    public class AuthenticationServices : IAuthentication
    {
        private readonly DumbsRajContext _appDbContext;
        private readonly UserManager<AppUser> _userManager;

        private readonly IJwtFactory _jwtFactory;
        private readonly JwtIssuerOptions _jwtOptions;

        private readonly IMapper _mapper;

        public AuthenticationServices(UserManager<AppUser> userManager, DumbsRajContext appDbContext, IMapper mapper, IJwtFactory jwtFactory, IOptions<JwtIssuerOptions> options)
        {
            _userManager = userManager;
            _appDbContext = appDbContext;
            _mapper = mapper;
            _jwtFactory = jwtFactory;
            _jwtOptions = options.Value;
        }

        public Task<bool> CreateUser(RegistrationViewModel model)
        {
            return createUser(model);
        }

        public async Task<bool> createUser(RegistrationViewModel model)
        {
            var userIdentity = _mapper.Map<AppUser>(model);

            //var userIdentity = new AppUser
            //{
            //    UserName = model.Username,
            //    FirstName = model.FirstName,
            //    LastName = model.LastName,
            //    Email = model.Email,
            //    EmailConfirmed = false,
            //    PhoneNumberConfirmed = false,
            //    TwoFactorEnabled = false,
            //    LockoutEnabled = false,
            //    AccessFailedCount = 0
            //};
            //var result = await _userManager.CreateAsync()

            var result = await _userManager.CreateAsync(userIdentity, model.Password);
            if (!result.Succeeded) return false;

            //await _appDbContext.Customers.AddAsync(new Customer { IdentityId = userIdentity.Id, Location = model.Location });
            await _appDbContext.SaveChangesAsync();
            return true;
        }

        public Task<ClaimsIdentity> GetClaimsIdentity(string userName, string password)
        {
            return asyncGetClaimsIdentity(userName, password);
        }

        public async Task<ClaimsIdentity> asyncGetClaimsIdentity(string userName, string password)
        {
            if (string.IsNullOrEmpty(userName) || string.IsNullOrEmpty(password))
                return await Task.FromResult<ClaimsIdentity>(null);

            // get the user to verifty
            var userToVerify = await _userManager.FindByNameAsync(userName);

            if (userToVerify == null) return await Task.FromResult<ClaimsIdentity>(null);

            // check the credentials
            if (await _userManager.CheckPasswordAsync(userToVerify, password))
            {
                return await Task.FromResult(_jwtFactory.GenerateClaimsIdentity(userName, userToVerify.Id));
            }
            // Credentials are invalid, or account doesn't exist
            return await Task.FromResult<ClaimsIdentity>(null);
        }
    }
}
