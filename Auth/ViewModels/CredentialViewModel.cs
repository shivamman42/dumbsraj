﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DumbsRaj.Auth.ViewModels
{

    //[Validation(typeof(CredentialsViewModelValidator))]
    public class CredentialViewModel
    {
        [Required]
        public string UserName { get; set; }
        [Required]
        public string Password { get; set; }
    }
}
