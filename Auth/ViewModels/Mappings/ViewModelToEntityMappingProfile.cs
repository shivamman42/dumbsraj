﻿using AutoMapper;
using DumbsRaj.Auth.ViewModels;
using DumbsRaj.persistence.Models.AppUser;

namespace Auth.ViewModels.Mappings
{
    public class ViewModelToEntityMappingProfile : Profile
    {
        public ViewModelToEntityMappingProfile()
        {
            CreateMap<RegistrationViewModel, AppUser>().ForMember(au => au.UserName, map => map.MapFrom(vm => vm.Username));
            CreateMap<AppUser, RegistrationViewModel>().ForMember(au => au.Username, map => map.MapFrom(vm => vm.UserName));
        }
    }
}