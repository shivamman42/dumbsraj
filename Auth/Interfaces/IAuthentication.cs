﻿using DumbsRaj.Auth.ViewModels;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Auth.Interfaces
{
    public interface IAuthentication
    {
        Task<bool> CreateUser(RegistrationViewModel model);
        Task<ClaimsIdentity> GetClaimsIdentity(string userName, string password);
    }
}
