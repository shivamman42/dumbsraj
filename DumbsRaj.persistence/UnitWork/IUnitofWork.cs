﻿using DumbsRaj.persistence.Models;
using DumbsRaj.persistence.Models.Movie;
using DumbsRaj.persistence.Models.Type;
using DumbsRaj.persistence.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace DumbsRaj.persistence.UnitWork
{
    public interface IUnitofWork
    {
        IRepository<Hint> HintRepository { get; }
        IRepository<Movies> MovieRepository { get; }
        IRepository<Questions> QuestionRepository { get; }
        IRepository<Types> TypeRepository { get; }

        void Save();
    }
}
