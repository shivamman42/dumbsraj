﻿using DataLayer.Repository;
using DumbsRaj.persistence.Models;
using DumbsRaj.persistence.Models.Movie;
using DumbsRaj.persistence.Models.Type;
using DumbsRaj.persistence.Repository;

namespace DumbsRaj.persistence.UnitWork
{
    public class UnitofWork : IUnitofWork
    {
        DumbsRajContext _context;
        IRepository<Hint> _hintRepository;
        IRepository<Movies> _moviesRepository;
        IRepository<Questions> _questionRepository;
        IRepository<Types> _typeRepository;

        public UnitofWork()
        {
            _context = new DumbsRajContext();
        }

        public UnitofWork(DumbsRajContext context)
        {
            _context = context;
        }

        public IRepository<Hint> HintRepository
        {
            get { return _hintRepository ?? (_hintRepository = new Repository<Hint>(_context)); }
        }

        public IRepository<Movies> MovieRepository
        {
            get { return _moviesRepository ?? (_moviesRepository = new Repository<Movies>(_context)); }
        }

        public IRepository<Questions> QuestionRepository
        {
            get { return _questionRepository ?? (_questionRepository = new Repository<Questions>(_context)); }
        }

        public IRepository<Types> TypeRepository
        {
            get { return _typeRepository ?? (_typeRepository = new Repository<Types>(_context)); }
        }

        public void Save()
        {
            _context.SaveChanges();
        }
    }
}
