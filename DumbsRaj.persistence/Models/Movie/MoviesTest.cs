﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace DumbsRaj.persistence.Models.Movie
{
    [TestFixture]
    public class MoviesTest
    {
        [TestCase]
        public void moviesSetTest()
        {
            var date = DateTime.Now.Date;
            var expectedValue = new Movies {
                AddedDate = date,
                Id = 1,
                Name = "Test",
                UpdatedDate = date
            };

            Movies movie = new Movies
            {
                AddedDate = expectedValue.AddedDate,
                Id = expectedValue.Id,
                Name = expectedValue.Name,
                UpdatedDate = expectedValue.UpdatedDate
            };

            Assert.AreEqual(movie.AddedDate, expectedValue.AddedDate);
            Assert.AreEqual(movie.Id, 1);
            Assert.AreEqual(movie.Name, "Test");
            Assert.AreEqual(movie.AddedDate, expectedValue.AddedDate);
        }
    }
}
