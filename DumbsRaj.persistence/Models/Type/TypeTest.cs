﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace DumbsRaj.persistence.Models.Type
{
    [TestFixture]
    public class TypeTest
    {
        [TestCase]
        public void hintSetTest() {
            DateTime date = DateTime.Now.Date;
            var expectedValue = new Types
            {
                AddedDate = date,
                Id = 1,
                Name = "Test",
                UpdatedDate = date
            };

            Types types = new Types
            {
                AddedDate = expectedValue.AddedDate,
                Id = expectedValue.Id,
                Name = expectedValue.Name,
                UpdatedDate = expectedValue.UpdatedDate
            };
            Assert.AreEqual(types.Id, 1);
            Assert.AreEqual(types.AddedDate, date);
            Assert.AreEqual(types.Name, "Test");
            Assert.AreEqual(types.UpdatedDate, date);
        }
    }
}
