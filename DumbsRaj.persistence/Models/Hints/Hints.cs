﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;


namespace DumbsRaj.persistence.Models
{
    public class Hint:Base
    {
        public string Description { get; set; }
        public int QuestionId { get; set; }

        [ForeignKey("QuestionId")]
        public Questions Question { get; set; }
        public string MediaUrl { get; set; }

       
    }
}
