﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using DumbsRaj.persistence.Models;

namespace DumbsRaj.persistence.Models.Hints
{
    [TestFixture]
    public class HintTest
    {
        [Test]
        public void TestSetHint() {
            DateTime date = DateTime.Now.Date;
            Hint expectedValue = new Hint
            {
                Id = 1,
                QuestionId = 1,
                AddedDate = date,
                UpdatedDate = null,
                Description = "Test",
                MediaUrl = "c:/Test"
            };

            Hint hint = new Hint
            {
                Id  = expectedValue.Id,
                QuestionId = expectedValue.QuestionId,
                AddedDate = expectedValue.AddedDate,
                UpdatedDate = expectedValue.UpdatedDate,
                Description = expectedValue.Description,
                MediaUrl = expectedValue.MediaUrl
            };

            Assert.AreEqual(hint.Id, 1);
            Assert.AreEqual(hint.QuestionId, 1);
            Assert.AreEqual(hint.AddedDate, expectedValue.AddedDate);
            Assert.AreEqual(hint.UpdatedDate, expectedValue.UpdatedDate);
            Assert.AreEqual(hint.Description, "Test");
            Assert.AreEqual(hint.MediaUrl, "c:/Test");
        }
    }
}
