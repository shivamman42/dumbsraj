﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using DumbsRaj.persistence.Models;

namespace DumbsRaj.persistence.Models.Test.Questions
{
    [TestFixture]
    public class QuestionTest
    {
        [TestCase]
        public void questionSetTes() {
            DateTime date = DateTime.Now.Date;

            var expectedValue = new DumbsRaj.persistence.Models.Questions
            {
                Id = 1,
                AddedDate  =date,
                UpdatedDate = null,
                Description  = "Test",
                Title = "Test",
                UserId = 1
            };
            DumbsRaj.persistence.Models.Questions questions = new DumbsRaj.persistence.Models.Questions
            {
                Id = expectedValue.Id,
                UserId = expectedValue.UserId,
                Title = expectedValue.Title,
                AddedDate = expectedValue.AddedDate,
                Description = expectedValue.Description,
                UpdatedDate = expectedValue.UpdatedDate
            };

            Assert.AreEqual(questions.Id, 1);
            Assert.AreEqual(questions.UserId, 1);
            Assert.AreEqual(questions.Title, "Test");
            Assert.AreEqual(questions.Description, "Test");
            Assert.AreEqual(questions.AddedDate, expectedValue.AddedDate);
            Assert.AreEqual(questions.UpdatedDate, null);
        }
    }
}
