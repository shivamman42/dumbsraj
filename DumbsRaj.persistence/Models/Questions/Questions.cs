﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DumbsRaj.persistence.Models
{
    public class Questions:Base
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public int  UserId { get; set; }
        public ICollection<Hint> Hints { get; set; }
    }
}
