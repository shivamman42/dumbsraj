﻿using DumbsRaj.persistence.Models.Movie;
using DumbsRaj.persistence.Models.Type;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace DumbsRaj.persistence.Models
{
    public class DumbsRajContext: IdentityDbContext<DumbsRaj.persistence.Models.AppUser.AppUser>
    {
        public DumbsRajContext()
        {

        }

        public DumbsRajContext(DbContextOptions<DumbsRajContext> options):base(options)
        {

        }
        //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //{
        //    if (!optionsBuilder.IsConfigured)
        //    {
        //        IConfigurationRoot configuration = new ConfigurationBuilder()
        //           .SetBasePath(Directory.GetCurrentDirectory())
        //           .AddJsonFile("appsettings.json")
        //           .Build();
        //        var connectionString = configuration.GetConnectionString("DbCoreConnectionString");
        //        optionsBuilder.UseSqlServer(connectionString);
        //    }
        //}

        public DbSet<Hint> Hints { get; set; }
        public DbSet<Movies> Movies { get; set; }
        public DbSet<Questions> Questions { get; set; }
        public DbSet<Types> Types { get; set; }
        //public DbSet<DumbsRaj.persistence.Models.AppUser.AppUser> AppUsers { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {

            if (!optionsBuilder.IsConfigured)
            {
                //#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=Shivam-IW;Initial Catalog=DumbsRaj;Persist Security Info=False;User ID=sa;Password=sa123##;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=True;Connection Timeout=30;");
            }
        }


    }
}
