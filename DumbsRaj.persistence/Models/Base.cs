﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DumbsRaj.persistence.Models
{
    public class Base
    {
        public int Id { get; set; }
        public DateTime AddedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
    }
}
