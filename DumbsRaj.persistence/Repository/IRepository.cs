﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DumbsRaj.persistence.Repository
{
    public interface IRepository<TEntity> : IDisposable where TEntity : class
    {
        void Create(TEntity entity);
        void UpdateData(TEntity entity);
        void Delete(object Id);
        void Delete(TEntity entity);
        IEnumerable<TEntity> All();
        TEntity GetById(object Id);
        void Update(TEntity entity, object Id);
        //void Create(global::BusinessLayer.ViewModels.TableVM tableVM);
        //void Create(global::BusinessLayer.ViewModels.TableVM tableVM);
    }
}
