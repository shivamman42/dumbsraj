﻿
using DumbsRaj.persistence.Models;
using DumbsRaj.persistence.Repository;
using DumbsRaj.persistence.UnitWork;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataLayer.Repository
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        private readonly DumbsRajContext _db;
        private readonly DbSet<TEntity> _dbSet;
        

        public Repository()
        {
            this._db = new DumbsRajContext();
            _dbSet = _db.Set<TEntity>();
        }

        public Repository(DumbsRajContext db)
        {
            db = new DumbsRajContext();
            this._db = db;
            this._dbSet = _db.Set<TEntity>();
        }

        public virtual void Create(TEntity entity)
        {
            if (entity != null)
            {
                _dbSet.Add(entity);
                _db.SaveChanges();
            }
        }

        public virtual void Delete(object id)
        {
            TEntity entityToDelete = _dbSet.Find(id);
            Delete(entityToDelete);
            _db.SaveChanges();
        }

        public virtual void Delete(TEntity entityToDelete)
        {
            _dbSet.Remove(entityToDelete);
            _db.SaveChanges();
        }

        public void Dispose()
        {
            _db.Dispose();
        }

        public IEnumerable<TEntity> All()
        {
            return _dbSet.AsEnumerable<TEntity>().AsEnumerable();
        }

        public virtual TEntity GetById(object Id)
        {
            return _dbSet.Find(Id);
        }

        public virtual void Update(TEntity entity, object Id)
        {
            var dataToUpdate = GetById(Id);
            //_dbSet.Update(dataToUpdate);
            //_db.SaveChanges();
            _db.Entry(dataToUpdate).State = EntityState.Modified;
            _db.SaveChanges();
        }

        public virtual void UpdateData(TEntity entity)
        {
            _dbSet.Update(entity);
            _db.SaveChanges();
            //_db.Entry(entity).State = EntityState.Modified;
            //_db.SaveChanges();
        }
    }
}
