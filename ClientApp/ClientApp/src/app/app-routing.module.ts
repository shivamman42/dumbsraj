import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';

const routes: Routes = [

  { path: "", redirectTo: '/home', pathMatch: 'full' },
  { path: "home", component: HomeComponent },
  // {
  //   path: 'home',
  //   children: [
  //     { path: "", component: FrontendComponent},
  //     { path: "ReadArticle/:id", component: ReadArticleComponent },
  //     // { path: "read-article", component: ReadArticleComponent },
  //   ]
  // },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
