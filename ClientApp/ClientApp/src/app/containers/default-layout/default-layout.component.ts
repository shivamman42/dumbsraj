import { Component, Input } from '@angular/core';
import { navItems } from './../../_nav';

@Component({
  selector: 'app-dashboard',
  templateUrl: './default-layout.component.html'
})
export class DefaultLayoutComponent {
  public navItems = navItems;
  public sidebarMinimized = true;
  private changes: MutationObserver;
  public element: HTMLElement = document.body;
  constructor() {

    this.changes = new MutationObserver((mutations) => {
      this.sidebarMinimized = document.body.classList.contains('sidebar-minimized');
    });

    this.changes.observe(<Element>this.element, {
      attributes: true
    });
  }

  logInWithGoogle() {
    // window.open('https://accounts.google.com/o/oauth2/autho?&response_type=token&display=popup&client_id=1036011556122-l86rhfv6qnjfl3c37fqqbeu9lrsu2lav.apps.googleusercontent.com=popup&redirect_uri=https://localhost:4200/oauth2callback',null,'width=600,height=400');
    var startPoint = 'https://accounts.google.com/o/oauth2/auth?scope=https://www.google.com/m8/feeds&'
    var client_id = 'client_id=1036011556122-l86rhfv6qnjfl3c37fqqbeu9lrsu2lav.apps.googleusercontent.com&'
    var redirect_uri = 'redirect_uri=https://localhost:4200/oauth2callback&'
    var response_type = 'response_type=token'
    
    var url = startPoint+client_id+redirect_uri+response_type;
    window.open(url,null,'width=600,height=400');
    // this.oauthSignIn();
  }

  oauthSignIn() {
    // Google's OAuth 2.0 endpoint for requesting an access token
    var oauth2Endpoint = 'https://accounts.google.com/o/oauth2/auth';

    // Create <form> element to submit parameters to OAuth 2.0 endpoint.
    var form = document.createElement('form');
    form.setAttribute('method', 'GET'); // Send as a GET request.
    form.setAttribute('action', oauth2Endpoint);

    // Parameters to pass to OAuth 2.0 endpoint.
    var params = {
      'client_id': '1036011556122-l86rhfv6qnjfl3c37fqqbeu9lrsu2lav.apps.googleusercontent.com',
      'redirect_uri': 'https://localhost:4200/oauth2callback',
      'response_type': 'token',
      'scope': 'https://www.googleapis.com/auth/drive.metadata.readonly',
      'include_granted_scopes': 'true',
      'state': 'pass-through value'
    };

    // Add form parameters as hidden input values.
    for (var p in params) {
      var input = document.createElement('input');
      input.setAttribute('type', 'hidden');
      input.setAttribute('name', p);
      input.setAttribute('value', params[p]);
      form.appendChild(input);
    }

    // Add form to page and submit it to open the OAuth 2.0 endpoint.
    document.body.appendChild(form);
    form.submit();
  }


  onSignIn(googleUser) {
    var profile = googleUser.getBasicProfile();
    console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
    console.log('Name: ' + profile.getName());
    console.log('Image URL: ' + profile.getImageUrl());
    console.log('Email: ' + profile.getEmail()); // This is null if the 'email' scope is not present.
  }

}
