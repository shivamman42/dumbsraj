﻿using System;
using System.Collections.Generic;
using System.Text;
using DumbsRaj.domain.Models.Type;
using DumbsRaj.persistence.Models.Type;

namespace DumbsRaj.Application.Mapper
{
    public static class TypeMapper
    {
        public static Types Convert(TypesVM data)
        {
            try
            {
                if (data == null)
                    return null;

                return new Types
                {
                    Id = data.Id,
                    AddedDate = data.AddedDate,
                    Name = data.Name,
                    UpdatedDate = data.UpdatedDate
                };

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public static TypesVM Convert(Types result)
        {
            try
            {
                if (result == null)
                    return null;

                return new TypesVM
                {
                    Id = result.Id,
                    AddedDate = result.AddedDate,
                    Name = result.Name,
                    UpdatedDate = result.UpdatedDate
                };
            }
            catch (Exception ex)
            {

                throw ex;
            }
            
        }

        public static List<TypesVM> Convert(List<Types> list)
        {
            try
            {
                if (list != null){
                    List<TypesVM> result = new List<TypesVM>();
                    foreach (var item in list)
                    {
                        result.Add(Convert(item));
                    }
                    return result;
                }
                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
