﻿using System;
using System.Collections.Generic;
using System.Text;
using DumbsRaj.domain.Models.Movie;
using DumbsRaj.persistence.Models.Movie;

namespace DumbsRaj.Application.Mapper
{
    public static class MovieMapper
    {
        public static Movies Convert(MoviesVM data)
        {
            try
            {
                if (data == null)
                    return null;

                return new Movies
                {
                    Id = data.Id,
                    AddedDate = data.AddedDate,
                    Name = data.Name,
                    UpdatedDate = data.UpdatedDate
                };
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public static MoviesVM Convert(Movies movies)
        {
            if (movies == null)
                return null;

            return new MoviesVM
            {
                Id = movies.Id,
                Name = movies.Name,
                AddedDate = movies.AddedDate,
                UpdatedDate = movies.UpdatedDate
            };
        }

        public static List<MoviesVM> Convert(List<Movies> list)
        {
            try
            {
                List<MoviesVM> data = new List<MoviesVM>();
                if (list != null) {
                    foreach (var item in list)
                    {
                        data.Add(Convert(item));
                    }
                }
                return data;
            }
            catch (Exception ex)
            {

                throw;
            }
        }
    }
}
