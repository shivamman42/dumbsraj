﻿using DumbsRaj.domain.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace DumbsRaj.Application.Interfaces
{
    public interface IHint
    {
        List<HintVM> List();
        bool Create(HintVM hints);
        bool Update(HintVM hints);
        HintVM GetById(int Id);
    }
}
