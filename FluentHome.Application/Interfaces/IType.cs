﻿using DumbsRaj.domain.Models.Type;
using System;
using System.Collections.Generic;
using System.Text;

namespace DumbsRaj.Application.Interfaces
{
    public interface IType
    {
        List<TypesVM> List();
        bool Create(TypesVM data);
        bool Update(TypesVM data);
        TypesVM GetById(int Id);
        bool Delete(int Id);
    }
}
