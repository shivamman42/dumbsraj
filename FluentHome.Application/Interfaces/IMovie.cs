﻿using DumbsRaj.domain.Models.Movie;
using DumbsRaj.domain.Models.Type;
using System;
using System.Collections.Generic;
using System.Text;

namespace DumbsRaj.Application.Interfaces
{
    public interface IMovie
    {
        List<MoviesVM> List();
        bool Create(MoviesVM data);
        bool Update(MoviesVM data);
        MoviesVM GetById(int Id);
        bool Delete(int id);
    }
}
