﻿using DumbsRaj.domain.Models;
using DumbsRaj.persistence.Models;
using DumbsRaj.persistence.UnitWork;
using DumbsRaj.Application.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace DumbsRaj.Application.Services
{
    public class HintServices : IHint
    {
        public IUnitofWork _unitOfWork;

        public HintServices()
        {
            _unitOfWork = new UnitofWork();
        }
        public bool Create(HintVM hints)
        {
            try
            {
                var data = new Hint
                {
                    Id = hints.Id,
                    AddedDate = hints.AddedDate,
                    Description = hints.Description,
                    MediaUrl = hints.MediaUrl,
                    QuestionId = hints.QuestionId,
                    UpdatedDate = hints.UpdatedDate
                };

                _unitOfWork.HintRepository.Create(data);
                return true;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public HintVM GetById(int Id)
        {
            try
            {
                return null;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public List<HintVM> List()
        {
            throw new NotImplementedException();
        }

        public bool Update(HintVM hints)
        {
            throw new NotImplementedException();
        }
    }
}
