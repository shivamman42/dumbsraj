﻿using DumbsRaj.domain.Models.Movie;
using DumbsRaj.persistence.Models;
using DumbsRaj.persistence.UnitWork;
using DumbsRaj.Application.Interfaces;
using DumbsRaj.Application.Mapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DumbsRaj.Application.Services
{
    public class MovieServices : IMovie
    {

        private IUnitofWork _unitOfWork;
        private DumbsRajContext _db;

        public MovieServices()
        {
            _db = new DumbsRajContext();
            _unitOfWork = new UnitofWork();
        }

        public bool Create(MoviesVM data)
        {
            try
            {
                //data.AddedDate = DateTime.Now;
                //_unitOfWork.MovieRepository.Create(MovieMapper.Convert(data));
                _db.Movies.Add(MovieMapper.Convert(data));
                _db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool Delete(int id)
        {
            try
            {
                _unitOfWork.MovieRepository.Delete(id);
                return true;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public MoviesVM GetById(int Id)
        {
            return MovieMapper.Convert(_unitOfWork.MovieRepository.GetById(Id));
        }

        public List<MoviesVM> List()
        {
            var list = _unitOfWork.MovieRepository.All();
            if (list != null)
                return MovieMapper.Convert(list.ToList());
            return null;
        }

        public bool Update(MoviesVM data)
        {
            var dataToUpdate = _unitOfWork.MovieRepository.GetById(data.Id);
            dataToUpdate.Id = data.Id;
            dataToUpdate.Name = data.Name;
            dataToUpdate.UpdatedDate = DateTime.Now;
            _unitOfWork.MovieRepository.UpdateData(dataToUpdate);
            return true;
        }
    }
}
