﻿using DumbsRaj.domain.Models.Type;
using DumbsRaj.persistence.Models.Type;
using DumbsRaj.persistence.UnitWork;
using DumbsRaj.Application.Interfaces;
using DumbsRaj.Application.Mapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DumbsRaj.Application.Services
{
    public class TypeServices : IType
    {
        private readonly IUnitofWork _unitofWork;
        public TypeServices()
        {
            //this._unitofWork = new UnitofWork();
            this._unitofWork = new UnitofWork();
        }

        public bool Create(TypesVM data)
        {
            try
            {
                data.AddedDate = DateTime.Now;
                _unitofWork.TypeRepository.Create(TypeMapper.Convert(data));
                return true;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public bool Delete(int Id)
        {
            try
            {
                _unitofWork.TypeRepository.Delete(Id);
                return true;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public TypesVM GetById(int Id)
        {
            try
            {
                return TypeMapper.Convert(_unitofWork.TypeRepository.GetById(Id));
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public List<TypesVM> List()
        {
            try
            {
                var list = _unitofWork.TypeRepository.All();
                if (list != null)
                    return TypeMapper.Convert(list.ToList());
                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool Update(TypesVM data)
        {
            try
            {
                var dataToUpdate = _unitofWork.TypeRepository.GetById(data.Id);
                dataToUpdate.Id = data.Id;
                dataToUpdate.Name = data.Name;
                dataToUpdate.UpdatedDate = DateTime.Now;
                _unitofWork.TypeRepository.UpdateData(dataToUpdate);
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
